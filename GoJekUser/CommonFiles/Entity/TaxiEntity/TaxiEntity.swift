//
//  TaxiEntity.swift
//  GoJekUser
//
//  Created by apple on 20/02/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import Foundation
import ObjectMapper

class TaxiReuqestEntity: Mappable {
    
    var use_wallet : Int?
    var schedule_date : String?
    var schedule_time : String?
    var someone: Int?
    var someone_name: String?
    var someone_email: String?
    var someone_mobile: String?
    var wheel_chair: Int?
    var child_seat: Int?
    var distance: Double?
    var promocode_id: Int?
    var isSchedule: Bool?
    var s_latitude: Double?
    var s_longitude: Double?
    var service_type:Int?
    var d_latitude: Double?
    var d_longitude: Double?
    var d_address: String?
    var d_latitude0: [Double]?
    var d_longitude0:[Double]?
    var d_address0: [String]?
    var payment_mode: String?
    var s_address: String?
    var rideTypeId: Int?
    var card_id: String?
    var max_amount : Int?
    var percentage : Int?
    var vehicle_type: TaxiServiceType?
    var rental_package_id: Int?
    var outstation_type: TaxiTrip?
    var depart_day: String?
    var depart_time: String?
    var return_day: String?
    var return_time: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
     func mapping(map: Map) {
        
        use_wallet <- map["use_wallet"]
        schedule_date <- map["schedule_date"]
        schedule_time <- map["schedule_time"]
        someone <- map["someone"]
        someone_email <- map["someone_email"]
        someone_name  <- map["someone_name"]
        wheel_chair <- map["wheel_chair"]
        child_seat <- map["child_seat"]
        distance <- map["distance"]
        s_latitude <- map["s_latitude"]
        s_longitude <- map["s_longitude"]
        service_type <- map["service_type"]
        d_latitude <- map["d_latitude"]
        d_longitude <- map["d_longitude"]
        d_address <- map["d_address"]
        d_latitude0 <- map["d_latitude"]
        d_longitude0 <- map["d_longitude"]
        d_address0 <- map["d_address"]
        payment_mode <- map["payment_mode"]
        max_amount <- map["max_amount"]
        percentage <- map["percentage"]
        vehicle_type <- map["vehicle_type"]
        rental_package_id <- map["rental_package_id"]
        outstation_type <- map["outstation_type"]
        depart_day <- map["depart_day"]
        depart_time <- map["depart_time"]
        return_day <- map["return_day"]
        return_time <- map["return_time"]
        s_address <- map["s_address"]
        
    }
}

