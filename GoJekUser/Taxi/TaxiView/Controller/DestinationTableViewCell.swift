//
//  DestinationTableViewCell.swift
//  GoJekUser
//
//  Created by Suren on 02/06/20.
//  Copyright © 2020 Appoets. All rights reserved.
//

import UIKit

class DestinationTableViewCell: UITableViewCell {

    @IBOutlet weak var destinationpinleading: NSLayoutConstraint!
    @IBOutlet weak var destinatioPinPointer: UIImageView!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var BackView: UIView!
    @IBOutlet weak var addMinus: UIButton!
    @IBOutlet weak var remove: UIButton!
    @IBOutlet weak var DestinationTf: UITextField!
    
    var onclickLikeButton: ((Bool)-> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
        DestinationTf.placeholder = TaxiConstant.enterDestination.localized
        DestinationTf.font = UIFont.setCustomFont(name: .medium, size: .x14)
        addMinus.titleLabel?.font = UIFont.setCustomFont(name: .medium, size: .x22)
        addMinus.setTitleColor(.appPrimaryColor,for: .normal)
        addMinus.tintColor = .appPrimaryColor
        remove.tintColor = .appPrimaryColor
        BackView.backgroundColor = UIColor(red: 248, green: 247, blue: 246, alpha: 1.0)
//        self.likeBtn.addTarget(self, action: #selector(destinationLikeAction), for: .touchUpInside)
        likeBtn.tintColor = .appPrimaryColor
//        likeBtn.setImage(UIImage(named: Constant.LikeEmpty), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
//    @objc func destinationLikeAction(){
//             if likeBtn.imageView?.image == UIImage(named: Constant.LikeEmpty){
//                 likeBtn.setImage(UIImage(named: Constant.LikeFull), for: .normal)
//                 onclickLikeButton!(true)
//
//             }else{
//                 likeBtn.setImage(UIImage(named: Constant.LikeEmpty), for: .normal)
//             }
//
//         }

}
