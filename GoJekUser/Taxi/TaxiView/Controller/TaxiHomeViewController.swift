//
//  TaxiHomeViewController.swift
//  GoJekUser
//
//  Created by Ansar on 10/06/19.
//  Copyright © 2019 Appoets. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SDWebImage
class TaxiHomeViewController: UIViewController {
    
    @IBOutlet weak var centerAddBtn: UIButton!
    @IBOutlet weak var destinationTableview: UITableView!
    @IBOutlet weak var DestinationViewConstrain: NSLayoutConstraint!
    @IBOutlet weak var addressViewConstrain: NSLayoutConstraint!
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var sourceView:UIView!
//    @IBOutlet weak var destinationView:UIView!
    @IBOutlet weak var addressBGView:UIView!
    @IBOutlet weak var viewCurrentLocation: UIView!
    @IBOutlet weak var backButton:UIButton!
    @IBOutlet weak var sourceTextField: UITextField!
    var IspolylineChange = false
    var rideTypeId:Int? //getting from menu list
    var fromChatNotification: Bool? = false
    var riderStatus: TaxiRideStatus = .none
    var serviceList: [Services]? = []
    var paymentMode: PaymentType?
    var reasonData: [ReasonData]?
    var isInvoiceShowed: Bool? = false
    var xmapView: XMapView?
    var selectedCard = CardResponseData()
    var serviceSelectionView: ServiceSelectionView?
    var loaderView: LoaderView?
    var tableView: CustomTableView?
    var invoiceView: InvoiceView?
    var rateCardView: RateCardView?
    var ratingView: RatingView?
    var rideDetailView: RideDetailView?
    var rideStatusView: RideStatusView?
    var floatyButton: FloatyButton?
    var sosButton: UIButton?
    var isFromOrderPage = false
    var isAppPresentTapOnPush:Bool = false // avoiding multiple screens redirectns,if same push comes multiple times
    var isChatAlreadyPresented:Bool = false
    var isAppFrom =  false
    var currentLocationImage = UIImageView()
    var newRequest = false
    var taxitype:TaxiServiceType = .ride
    var outstationType:TaxiTrip = .oneWay
    var schedulEntity : TaxiReuqestEntity?
    var IsAddaStop = false
    var DestintionArray : [String] = [String]()
    var IsClickLocation = false
    var sourceLocationDetail = SourceDestinationLocation() {
        didSet {
            DispatchQueue.main.async {
                self.sourceTextField.text = self.sourceLocationDetail.address
            }
        }
    }
    var depart_day = "0"
    var depart_time = "0"
    var return_day = "0"
    var return_time = "0"
    var scheduleNowView: ScheduleView?
    var DestintionLattiLangi = [CLLocationCoordinate2D]()
    var DestintionLatti = [Double]()
    var DestintionLangi = [Double]()
    var Destination  = ""

    var lat = 0.0
    var long = 0.0
    
    var packageArr:[Rental_packages] = []
    var destinationLocationDetail = SourceDestinationLocation() {
        didSet {
            
            
            print (UserDefaults.standard.integer(forKey:"INDEX") as Int)
            
            //self.destinationTextField.text = self.destinationLocationDetail.address
          if  DestintionArray.count > 0 {
        if self.riderStatus == .none{
            
            DestintionLattiLangi[UserDefaults.standard.integer(forKey:"INDEX") as Int] = (self.destinationLocationDetail.locationCoordinate ?? CLLocationCoordinate2D.init(latitude: 0.0, longitude: 0.0))
            DestintionArray[UserDefaults.standard.integer(forKey:"INDEX") as Int] = (self.destinationLocationDetail.address ?? "")
            self.DestintionLatti[UserDefaults.standard.integer(forKey:"INDEX") as Int] = self.destinationLocationDetail.locationCoordinate?.latitude ?? 0.0
            self.DestintionLangi[UserDefaults.standard.integer(forKey:"INDEX") as Int] = self.destinationLocationDetail.locationCoordinate?.longitude ?? 0.0
            
            self.destinationTableview.reloadData()
            if DestintionLattiLangi.first?.latitude != 0 && self.DestintionLattiLangi.first?.longitude  != 0 {
               if self.riderStatus == .none{
                    self.WhenEnterDestination()
                }
             }
         }
            
            
        }
        }
    }
    
    var currentRequest : RequestResponseData? {
        didSet {
            DispatchQueue.main.async {
                if (self.currentRequest?.data?.count ?? 0) > 0 {
                    self.handleRequest()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoads()
        isAppFrom = true
//        destinationTableview?.transform = CGAffineTransform(scaleX: 1, y: -1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            self.hideTabBar()
        self.viewWillAppearCustom()
        XCurrentLocation.shared.latitude = 0
        XCurrentLocation.shared.longitude = 0
        isAppPresentTapOnPush = false
        ChatPushClick.shared.isPushClick = true
        //For chat
        NotificationCenter.default.addObserver(self, selector: #selector(isChatPushRedirection), name: Notification.Name(pushNotificationType.chat_transport.rawValue), object: nil)
        BackGroundRequestManager.share.stopBackGroundRequest()
      //  socketSettingUpdate()
        self.socketAndBgTaskSetUp()
        
        
        FLocationManager.shared.start { (info) in
           
            self.lat = info.latitude ?? 0.0
            self.long = info.longitude ?? 0.0
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.removeMapView()
        scheduleNowView = nil
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.viewWillLayoutSubviewCustom()
    }
    
}

//MARK: - Methods
extension TaxiHomeViewController {
    private func initialLoads() {
        self.localization()
        setFont()
        self.destinationTableview.delegate = self
        self.destinationTableview.dataSource = self
        DestintionArray.append(TaxiConstant.enterDestination.localized)
        DestintionLattiLangi.append(CLLocationCoordinate2D.init(latitude: 0.0, longitude: 0.0))
        DestintionLangi.append(0.00)
        DestintionLatti.append(0.00)
        self.destinationTableview.reloadInMainThread()
        mapView.backgroundColor = .whiteColor
        self.backButton.addTarget(self, action: #selector(tapBack), for: .touchUpInside)
        // for to get cancel reasons list
        //BackGroundRequestManager.share.stopBackGroundRequest()
        //self.socketAndBgTaskSetUp()
        currentLocationImage.image = UIImage(named: TaxiConstant.car_marker)?.resizeImage(newWidth: 25)
        let locationViewGesture = UITapGestureRecognizer(target: self, action: #selector(tapCurrentLocation(_:)))
        self.viewCurrentLocation.addGestureRecognizer(locationViewGesture)
        NotificationCenter.default.addObserver(self, selector: #selector(enterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        sourceTextField.backgroundColor = .boxColor
//        destinationTextField.backgroundColor = .boxColor
        sourceTextField.textColor = .blackColor
//        destinationTextField.textColor = .blackColor
        sourceView.backgroundColor = .boxColor
//        destinationView.backgroundColor = .boxColor
        mapView.backgroundColor = .whiteColor
    }
    
    @objc private func enterForeground() {
        
        if let _ = currentRequest {
            currentRequest = nil
        }
        isAppFrom = false
        BackGroundRequestManager.share.resetBackGroudTask()
        socketAndBgTaskSetUp()
    }
    
    private func viewWillAppearCustom() {
        
        self.navigationController?.isNavigationBarHidden = true
        self.addMapView()
        if CommonFunction.checkisRTL() {
            //self.backButton.imageView?.image = UIImage.init(named: Constant.ic_back)?.ro
            self.backButton.imageView!.transform = self.backButton.imageView!.transform.rotated(by: .pi)
        }else {
            self.backButton.imageView?.image = UIImage.init(named: Constant.ic_back)
        }
        self.backButton.tintColor = .blackColor
        self.xmapView?.didUpdateLocation = { [weak self] (location) in
            guard let self = self else {
                return
            }
            self.xmapView?.locationUpdate = self
            print("current location : \(location)")
            let currentLocation = CLLocation(latitude: location.latitude ?? 0, longitude: location.longitude ?? 0)
            if self.sourceLocationDetail.locationCoordinate == nil {
                self.xmapView?.currentLocation = currentLocation
                self.getCurrentLocationDetails()
            }else{
                self.xmapView?.moveCameraPosition(lat: self.sourceLocationDetail.locationCoordinate?.latitude ?? APPConstant.defaultMapLocation.latitude, lng: self.sourceLocationDetail.locationCoordinate?.longitude ?? APPConstant.defaultMapLocation.longitude)
                DispatchQueue.main.async {
                    self.setSourceDestinationAndPolyline()
                }
            }
        }
        getReasons()
     
        
        if fromChatNotification == true {
            fromChatNotification = false
            ChatPushClick.shared.isPushClick = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                if self.isAppPresentTapOnPush == false {
                    self.isAppPresentTapOnPush = true
                    self.isChatAlreadyPresented = false
                }
                else {
                    self.isChatAlreadyPresented = true
                }
                self.navigationToChatView()
            }
        }
    }
    
    private func viewWillLayoutSubviewCustom() {
        addressBGView.addShadow(radius: 8, color: .lightGray)
//        destinationView.addShadow(radius: 0, color: .lightGray)
        viewCurrentLocation.setRadiusWithShadow()
        self.xmapView?.frame = self.mapView.bounds
        DispatchQueue.main.async {
            self.sosButton?.setCornerRadius()
        }
    }
    
    //For chat
    @objc func isChatPushRedirection() {
        
        if isAppPresentTapOnPush == false {
            if isAppFrom == true {
                
                if self.isAppPresentTapOnPush == false {
                    self.isAppPresentTapOnPush = true
                    self.isChatAlreadyPresented = false
                }
                else {
                    self.isChatAlreadyPresented = true
                }
                self.navigationToChatView()
            }else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                    if self.isAppPresentTapOnPush == false {
                        self.isAppPresentTapOnPush = true
                        self.isChatAlreadyPresented = false
                    }
                    else {
                        self.isChatAlreadyPresented = true
                    }
                    //  self.isChatAlreadyPresented = true
                    self.navigationToChatView()
                }
                
            }
        }else{
            
        }
    }
    
    private func localization() {
        sourceTextField.placeholder = TaxiConstant.enterSource.localized
//        destinationTextField.placeholder = TaxiConstant.enterDestination.localized
    }
    
    private func setFont() {
        sourceTextField.font = UIFont.setCustomFont(name: .medium, size: .x14)
//        destinationTextField.font = UIFont.setCustomFont(name: .medium, size: .x14)
    }
    
    //Getting current location detail
    private func getCurrentLocationDetails() {
        xmapView?.getCurrentLocationDetail { [weak self] (location) in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async {
                if let cLocation = self.sourceLocationDetail.locationCoordinate,cLocation.latitude == 0 {
                    self.storeCurrentLocation(location: location)
                } else {
                    self.storeCurrentLocation(location: location)
                    if (self.currentRequest?.data?.count ?? 0) == 0 {
                        self.getServiceListAPI()
                    }
                }
            }
        }
    }
    
    private func setSourceDestinationAndPolyline() {
        if (currentRequest?.data?.count ?? 0) > 0 {
            handleRequest()
            return
        }
        getServiceListAPI()
        DispatchQueue.main.async {
            self.setSourceDestinationMarker()
        }
//        if !(sourceTextField.text?.isEmpty ?? false) && !(destinationTextField.text?.isEmpty ?? false) {
//
//            //My location
//            let myLocation = CLLocation(latitude: (self.sourceLocationDetail.locationCoordinate?.latitude ?? 0.0), longitude: (self.sourceLocationDetail.locationCoordinate?.longitude ?? 0.0))
//
//            //My buddy's location
//            let myBuddysLocation = CLLocation(latitude: (self.destinationLocationDetail.locationCoordinate?.latitude ?? 0.0), longitude: (self.destinationLocationDetail.locationCoordinate?.longitude ?? 0.0))
//
//            //Measuring my distance to my buddy's (in km)
//            let distance = myLocation.distance(from: myBuddysLocation) / 1000
//
//            let disStr = String(format: "The distance to my buddy is %.01f", distance)
//
//            //Display the result in km
//            print(String(format: "The distance to my buddy is %.01f", distance))
//            if self.taxitype == .outstation {
//                if (distance) < 100.0 {
//                    ToastManager.show(title: TaxiConstant.noservice.localized, state: .error)
//                   // serviceSelectionView?.getPricingButton.isUserInteractionEnabled = false
//                }else{
//                   // serviceSelectionView?.getPricingButton.isUserInteractionEnabled = true
//                    getServiceListAPI()
//                    DispatchQueue.main.async {
//                        self.setSourceDestinationMarker()
//                        self.xmapView?.drawPolyLineFromSourceToDestination(source: self.sourceLocationDetail.locationCoordinate ?? APPConstant.defaultMapLocation, destination: self.destinationLocationDetail.locationCoordinate ?? APPConstant.defaultMapLocation, lineColor: .taxiColor)
//                        //   self.xmapView?.showCurrentLocation()
//
//                    }
//                }
//            }else{
//
//                getServiceListAPI()
//                DispatchQueue.main.async {
//                    self.setSourceDestinationMarker()
//                    self.xmapView?.drawPolyLineFromSourceToDestination(source: self.sourceLocationDetail.locationCoordinate ?? APPConstant.defaultMapLocation, destination: self.destinationLocationDetail.locationCoordinate ?? APPConstant.defaultMapLocation, lineColor: .taxiColor)
//                    //   self.xmapView?.showCurrentLocation()
//
//                }
//            }
//        }
    }
    
    private func redirectToGoogleMap() {
        let baseUrl = "comgooglemaps-x-callback://"
        if UIApplication.shared.canOpenURL(URL(string: baseUrl)!) {
            
            let checkRequestDetail = currentRequest?.data?.first
            let directionsRequest = "comgooglemaps://?saddr=\(lat),\(long)&daddr=\(checkRequestDetail?.d_latitude ?? 0.0),\(checkRequestDetail?.d_longitude ?? 0.0)&directionsmode=driving"
            if let url = URL(string: directionsRequest) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        else {
            ToastManager.show(title: Constant.googleConstant.localized, state: .error)
        }
    }
    
    
    private func addMapView() {
        xmapView = XMapView(frame: self.mapView.bounds)
        xmapView?.tag = 100
        guard let _ = xmapView else {
            return
        }
        self.mapView.addSubview(self.xmapView!)
        self.mapView.bringSubviewToFront(self.floatyButton ?? UIButton()) // bring to front view if goes to chat and come back in between flow
        self.mapView.bringSubviewToFront(self.sosButton ?? UIButton())
        self.xmapView?.didDragMap = { [weak self] (isDrag,_) in
            guard let self = self else {
                return
            }
            self.serviceSelectionView?.isHidden = isDrag
            self.rateCardView?.isHidden = isDrag
            self.rideDetailView?.isHidden = isDrag
            self.rideStatusView?.isHidden = isDrag
            self.floatyButton?.isHidden = isDrag
            self.sosButton?.isHidden = isDrag
        }
    }
    
    private func removeMapView() {
        for subView in mapView.subviews where subView.tag == 100 {
            xmapView?.clearAll()
            subView.removeFromSuperview()
            xmapView?.currentLocation = nil
            xmapView = nil
        }
    }
    
    private func setMenuButton() {
        if let _ = self.getSosButton() {
            self.sosButton?.setTitle(TaxiConstant.sos, for: .normal)
            self.sosButton?.addTarget(self, action: #selector(sosButtonAction(_:)), for: .touchUpInside)
            self.sosButton?.titleLabel?.font = UIFont.setCustomFont(name: .bold, size: .x14)
            self.sosButton?.setTitleColor(.white, for: .normal)
            self.sosButton?.backgroundColor = .black
        }
        if let _ = getFloatyView() {
            floatyButton?.buttonOneImage = UIImage(named: Constant.phoneImage)?.imageTintColor(color1: .white) ?? UIImage()
            floatyButton?.buttonTwoImage = UIImage(named: Constant.chatImage)?.imageTintColor(color1: .white) ?? UIImage()
            floatyButton?.bgColor = .taxiColor
            floatyButton?.backgroundColor = .black
            floatyButton?.onTapButtonOne = { [weak self] in
                guard let self = self else {
                    return
                }
                if let phoneNumber = self.currentRequest?.data?.first?.provider?.mobile {
                    AppUtils.shared.call(to: phoneNumber)
                }
            }
            floatyButton?.onTapButtonTwo = { [weak self] in
                guard let self = self else {
                    return
                }
                self.navigationToChatView()
            }
        }
    }
    
    @objc func sosButtonAction(_ sender: UIButton) {
        if let phoneNumber = currentRequest?.sos {
            AppUtils.shared.call(to: phoneNumber)
        }
    }
    
    func navigationToChatView() {
        
        let checkRequestDetail = currentRequest?.data?.first
        let providerDetail = checkRequestDetail?.provider
        let userDetail = checkRequestDetail?.user
        let serviceDetail = checkRequestDetail?.service_type
        let chatView = ChatViewController()
        chatView.requestId = "\((checkRequestDetail?.id ?? 0))"
        chatView.chatRequestFrom = MasterServices.Transport.rawValue
        chatView.userId = "\((userDetail?.id ?? 0))"
        chatView.userName = "\( userDetail?.firstName ?? "")" + " " + "\(userDetail?.lastName ?? "")"
        chatView.providerId = "\((providerDetail?.id ?? 0))"
        chatView.providerName = "\(providerDetail?.first_name ?? "")" + " " + "\(providerDetail?.last_name ?? "")"
        chatView.adminServiceId = serviceDetail?.admin_service_id
        chatView.isChatPresented = isChatAlreadyPresented
        self.navigationController?.pushViewController(chatView, animated: true)
    }
    
    private func getFloatyView() -> FloatyButton? {
        guard let _ = floatyButton else {
            floatyButton = FloatyButton()
            mapView.addSubview(self.floatyButton ?? UIView())
            if let taxiView = self.rideStatusView {
                floatyButton?.translatesAutoresizingMaskIntoConstraints = false
                floatyButton?.bottomAnchor.constraint(equalTo: taxiView.topAnchor, constant: -10).isActive = true
                floatyButton?.rightAnchor.constraint(equalTo: taxiView.rightAnchor, constant: -10).isActive = true
                floatyButton?.heightAnchor.constraint(equalTo: taxiView.widthAnchor, multiplier: 0.12).isActive = true
                floatyButton?.widthAnchor.constraint(equalTo: taxiView.widthAnchor, multiplier: 0.12).isActive = true
            }
            return floatyButton
        }
        return floatyButton
    }
    
    private func getSosButton() -> UIButton? {
        guard let _ = sosButton else {
            sosButton = UIButton()
            mapView.addSubview(self.sosButton ?? UIButton())
            if let taxiView = self.rideStatusView {
                sosButton?.translatesAutoresizingMaskIntoConstraints = false
                sosButton?.bottomAnchor.constraint(equalTo: taxiView.topAnchor, constant: -10).isActive = true
                sosButton?.leftAnchor.constraint(equalTo: taxiView.leftAnchor, constant: 10).isActive = true
                sosButton?.heightAnchor.constraint(equalTo: taxiView.widthAnchor, multiplier: 0.12).isActive = true
                sosButton?.widthAnchor.constraint(equalTo: taxiView.widthAnchor, multiplier: 0.12).isActive = true
            }
            return sosButton
        }
        return self.sosButton
    }
    
    func storeCurrentLocation(location:LocationDetail) {
        self.sourceLocationDetail = SourceDestinationLocation(address: location.address,locationCoordinate: LocationCoordinate(latitude: (location.coordinate.latitude), longitude: (location.coordinate.longitude)))
        self.xmapView?.showCurrentLocation()
    }
    
    func WhenEnterDestination(){
        let tempId = "\(AppManager.shared.getSelectedServices()?.menu_type_id ?? 0)"
        let param: Parameters = [TaxiConstant.type:1 ,//tempId
                                 AccountConstant.latitude: self.sourceLocationDetail.locationCoordinate?.latitude ?? 0.00,
                                 AccountConstant.longitude: self.sourceLocationDetail.locationCoordinate?.longitude ?? 0.00,
                                 AccountConstant.Dlatitude: self.DestintionLattiLangi.first?.latitude ?? 0.00,
                                 AccountConstant.Dlongitude: self.DestintionLattiLangi.first?.longitude ?? 0.00,
                                 LoginConstant.city_id: guestAccountCity]
        self.taxiPresenter?.getServiceList(param: param, showLoader: true)
    }
    
    func socketAndBgTaskSetUp() {
        if let requestData = currentRequest,requestData.data?.first?.id ?? 0 != 0 {
            BackGroundRequestManager.share.startBackGroundRequest(type: .ModuleWise, roomId: SocketUtitils.construtRoomKey(requestID: "\(requestData.data?.first?.id ?? 0)", serviceType: .transport), listener: .Transport)
            BackGroundRequestManager.share.requestCallback =  { [weak self] in
                guard let self = self else {
                    return
                }
                self.checkRequestApiCall()
            }
        } else {
            checkRequestApiCall()
        }
    }
    func checkRequestApiCall() {
        self.taxiPresenter?.checkTaxiRequest()
    }
    
    private func getServiceListAPI() {
        self.getServiceList(location: self.sourceLocationDetail.locationCoordinate ?? CLLocationCoordinate2D())
    }
    
    @objc func tapCurrentLocation(_ sender: UITapGestureRecognizer) {
        xmapView?.showCurrentLocation()
    }
    
    private func getReasons() {
        self.taxiPresenter?.getReasons(param: [TaxiConstant.type: ServiceType.trips.currentType])
    }
    
    func getServiceList(location: LocationCoordinate) {
        if location.latitude == 0 {
            getCurrentLocationDetails()
        }else if (serviceList?.count ?? 0 > 0) && riderStatus == .none {
            showServiceList()
        }else {
            let tempId = "\(AppManager.shared.getSelectedServices()?.menu_type_id ?? 0)"
            let param: Parameters = [Constant.Ptype: tempId,
                                     AccountConstant.latitude: location.latitude,
                                     AccountConstant.longitude: location.longitude,
                                     LoginConstant.city_id: guestAccountCity,
                                     TaxiConstant.Pvehicle_type: taxitype.currentType]
            self.taxiPresenter?.getServiceList(param: param, showLoader: false)
        }
    }
    
    private func handleRequest() {
        guard let status = currentRequest?.data?.first?.status, currentRequest?.data?.first?.id != nil else {
            self.riderStatus = .none
            UserDefaults.standard.set(0, forKey:"INDEX")
            self.DestintionArray.removeAll()
            self.DestintionLattiLangi.removeAll()
            DestintionArray.append(TaxiConstant.enterDestination.localized)
            DestintionLattiLangi.append(CLLocationCoordinate2D.init(latitude: 0.0, longitude: 0.0))
            DestintionLangi.append(0.00)
            DestintionLatti.append(0.00)
            self.addressViewConstrain.constant = 102
            self.destinationTableview.reloadData()
            self.removeAllView()
            return
        }
        self.sourceLocationDetail = SourceDestinationLocation(address: currentRequest?.data?.first?.s_address ?? "", locationCoordinate: CLLocationCoordinate2D(latitude: currentRequest?.data?.first?.s_latitude ?? 0, longitude: currentRequest?.data?.first?.s_longitude ?? 0))
        
     //   self.destinationLocationDetail = SourceDestinationLocation(address: currentRequest?.data?.first?.d_address ?? "", locationCoordinate: CLLocationCoordinate2D(latitude: currentRequest?.data?.first?.d_latitude ?? 0, longitude: currentRequest?.data?.first?.d_longitude ?? 0))
        
        //        self.xmapView?.removeLocationMarkers()
       // self.DestintionLattiLangi.removeAll()
       // self.DestintionArray.removeAll()
        for D in 0..<(currentRequest?.data?.first?.drop_requests?.count ?? 0){
            self.DestintionArray.append(currentRequest?.data?.first?.drop_requests?[D].d_address ?? "")
            self.DestintionLattiLangi.append(CLLocationCoordinate2D(latitude: currentRequest?.data?.first?.drop_requests?[D].d_latitude ?? 0, longitude: currentRequest?.data?.first?.drop_requests?[D].d_longitude ?? 0))
        }
        if let markerURL = URL(string: currentRequest?.data?.first?.ride?.vehicle_marker ?? "") {
            self.currentLocationImage.load(url: markerURL, completion: { (image) in
                DispatchQueue.main.async {
                    self.xmapView?.currentLocationMarkerImage = image.resizeImage(newWidth: 30)
                    self.xmapView?.setProviderCurrentLocationMarkerPosition(coordinate: self.sourceLocationDetail.locationCoordinate ?? CLLocationCoordinate2D())
                    //self.xmapView?.setDestinationLocationMarker(destinationCoordinate: self.destinationLocationDetail.locationCoordinate ?? APPConstant.defaultMapLocation, marker: UIImage(named: Constant.destinationPin) ?? UIImage())
                }
            })
            
        }else{
            self.xmapView?.currentLocationMarkerImage = self.currentLocationImage.image
        }
        self.riderStatus = TaxiRideStatus(rawValue: status) ?? TaxiRideStatus.none
        print("##### \(status)")
        switch riderStatus {
        case .searching:
            handleSearch()
        case .accepted, .pickedup, .started, .arrived :
           // IspolylineChange = true
            setSourceDestinationMarker()
           
            handleOnRide(with: status)
        case .dropped:
            handleDropped()
        case .completed:
            handleComplete()
        default:
            UserDefaults.standard.set(0, forKey:"INDEX")
            self.DestintionArray.removeAll()
            self.DestintionLattiLangi.removeAll()
            DestintionArray.append(TaxiConstant.enterDestination.localized)
            DestintionLattiLangi.append(CLLocationCoordinate2D.init(latitude: 0.0, longitude: 0.0))
            DestintionLangi.append(0.00)
            DestintionLatti.append(0.00)
            self.addressViewConstrain.constant = 102
            self.destinationTableview.reloadData()
            removeAllView()
        }
        addressBGView.isHidden = ![TaxiRideStatus.none].contains(riderStatus)
        if [TaxiRideStatus.searching,TaxiRideStatus.completed,TaxiRideStatus.none].contains(riderStatus) {
            self.xmapView?.isVisibleCurrentLocation(visible: true)
        }else{
            self.xmapView?.isVisibleCurrentLocation(visible: false)
        }
    }
    
    @objc func extendTripSelect() {
        AppAlert.shared.simpleAlert(view: self, title: "", message: Constant.extendTripAlert.localized, buttonOneTitle: Constant.SYes.localized, buttonTwoTitle: Constant.SNo.localized)
        AppAlert.shared.onTapAction = { [weak self] tag in
            guard let self = self else {
                return
            }
            if tag == 0 {
                let locationView = TaxiRouter.taxiStoryboard.instantiateViewController(withIdentifier: TaxiConstant.LocationSelectionController) as! LocationSelectionController
                locationView.isSource = false
                locationView.isExtendTrip = true
                locationView.isExtendTripSelectedLocation = { [weak self] addressData in
                    guard let self = self else {
                        return
                    }
                    // print(addressData)
                    let extendParams:Parameters = [TaxiConstant.id: self.currentRequest?.data?.first?.id ?? 0,
                                                   TaxiConstant.latitude: addressData.locationCoordinate?.latitude ?? 0.0,
                                                   TaxiConstant.longitude: addressData.locationCoordinate?.longitude ?? 0.0,
                                                   TaxiConstant.address: addressData.address ?? ""]
                    self.taxiPresenter?.extendTrip(param: extendParams)
                }
                self.navigationController?.pushViewController(locationView, animated: true)
            }
        }
    }
    
    // change payment in invoice
    @objc func changeInvoicePaymentTapped() {
        
        let paymentVC = AccountRouter.accountStoryboard.instantiateViewController(withIdentifier: AccountConstant.PaymentSelectViewController) as! PaymentSelectViewController
        paymentVC.isChangePayment = true
        paymentVC.onClickPayment = { [weak self] (type,cardEntity) in
            guard let self = self else {
                return
            }
            self.paymentMode = type
            self.invoiceView?.paymentType = type
            if type == .CASH {
                self.invoiceView?.cardCashLabel.text = type.rawValue
                self.invoiceView?.paymentImage.image = PaymentType(rawValue: type.rawValue)?.image
            } else {
                self.selectedCard = cardEntity!
                self.invoiceView?.cardCashLabel.text = Constant.cardPrefix + cardEntity!.last_four!
                self.invoiceView?.paymentImage.image = PaymentType(rawValue: type.rawValue)?.image
            }
            
            self.taxiPresenter?.updatePayment(param: [TaxiConstant.id: self.currentRequest?.data?.first?.id ?? 0,
                                                      TaxiConstant.payment_mode: self.paymentMode?.rawValue ?? PaymentType.CASH.rawValue,
                                                      TaxiConstant.card_id: self.selectedCard.card_id ?? ""])
        }
        self.navigationController?.pushViewController(paymentVC, animated: true)
    }
    
    private func showDimView(view: UIView) {
        let dimView = UIView(frame: self.view.frame)
        dimView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        dimView.addSubview(view)
        self.view.addSubview(dimView)
    }
    
    @objc func tapBack() {
         ChatPushClick.shared.isPushClick = false
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func tapCurrentLocation() {
        DispatchQueue.main.async {
            self.xmapView?.showCurrentLocation()
        }
    }
}

//MARK: UI Methods

extension TaxiHomeViewController {
    
    @objc func tapScheduleNow(date: Int) {
        schedulEntity = TaxiReuqestEntity()

        if scheduleNowView == nil, let scheduleView = Bundle.main.loadNibNamed(TaxiConstant.ScheduleView, owner: self, options: [:])?.first as? ScheduleView {
            let viewHeight = (view.frame.height/100)*25
            scheduleView.frame = CGRect(origin: CGPoint(x: 0, y: view.frame.height-viewHeight), size: CGSize(width: view.frame.width, height: viewHeight))
            self.scheduleNowView = scheduleView
            scheduleView.show(with: .bottom, completion: nil)
            showDimView(view: scheduleView)
        }
        scheduleNowView?.scheduleButton.setTitle(Constant.SDone.localized, for: .normal)
        scheduleNowView?.onClickScheduleNow = { [weak self] (selectedDate,selectedTime) in
            guard let self = self else {
                return
            }
            if selectedDate == "" && selectedTime == "" {
                AppAlert.shared.simpleAlert(view: self, title: "", message: Constant.dateTimeSelect.localized, buttonTitle: "OK")
                return
            }
            
            if date == 0 {
                self.schedulEntity?.depart_day = selectedDate
                self.schedulEntity?.depart_time = selectedTime
                self.depart_day = selectedDate
                self.depart_time = selectedTime
                
                self.serviceSelectionView?.startSelectButton.setTitle("\(selectedDate) , \(selectedTime)", for: .normal)
            }else{
                self.schedulEntity?.return_day = selectedDate
                self.schedulEntity?.return_time = selectedTime
                self.return_day = selectedDate
                self.return_time = selectedTime
                self.serviceSelectionView?.endSelectButton.setTitle("\(selectedDate) , \(selectedTime)", for: .normal)
            }
            
            self.scheduleNowView?.superview?.removeFromSuperview() // remove dimview
            self.scheduleNowView?.dismissView(onCompletion: {
                self.scheduleNowView = nil
            })
                        
        }
    }
    
    func showServiceList() {
        
        if let _ = getServiceListView() {
            DispatchQueue.main.async {
               
                self.serviceSelectionView?.serviceDetails = self.serviceList ?? []
                self.serviceSelectionView?.packageArr = self.packageArr
               
                
            }
            
            let baseConfig = AppConfigurationManager.shared.baseConfigModel
            //Hide social signin
            if baseConfig?.responseData?.appsetting?.rental == 1 {
                self.serviceSelectionView?.rentalsButton.isHidden = false
            }else{
                self.serviceSelectionView?.rentalsButton.isHidden = true

            }
            
            if baseConfig?.responseData?.appsetting?.outstation == 1 {
                self.serviceSelectionView?.outstationButton.isHidden = false
            }else{
                self.serviceSelectionView?.outstationButton.isHidden = true

            }
            
            serviceSelectionView?.paymentChangeClick = { [weak self] (paymentModes,cardEntity) in
                guard let self = self else {
                    return
                }
                self.paymentMode = paymentModes
                self.selectedCard = cardEntity
                if (self.currentRequest?.data?.count ?? 0) > 0 {
                    self.taxiPresenter?.updatePayment(param: [TaxiConstant.id: self.currentRequest?.data?.first?.id ?? 0,
                                                              TaxiConstant.payment_mode: self.paymentMode?.rawValue ?? PaymentType.CASH.rawValue,
                                                              TaxiConstant.card_id: self.selectedCard.card_id ?? ""])
                }
            }
            
            serviceSelectionView?.tapBookingDate = { [weak self] (type,tag) in
                guard let self = self else {
                    return
                }
                self.outstationType = TaxiTrip(rawValue: type.rawValue)!
                self.tapScheduleNow(date: tag)
            }
            
            serviceSelectionView?.tapPackage = { [weak self] (type) in
                guard let self = self else {
                    return
                }
//                self.destinationTextField.text = ""
                self.destinationLocationDetail = SourceDestinationLocation()

                self.taxitype = TaxiServiceType(rawValue: type.rawValue)!
                if type == .rental {
//                    self.destinationView.isHidden = true
                }else{
//                    self.destinationView.isHidden = false
                }
                self.serviceAPIList(type: type, loader: true)
                
            }
            serviceSelectionView?.tapService = { [weak self] (servicesData) in
                guard let self = self else {
                    return
                }
                if let serviceView = self.serviceSelectionView {
                    if (self.removeView(viewObj: serviceView)) {
                        self.serviceSelectionView = nil
                    }
                    self.showRateCard(serviceData: servicesData)
                }
                
            }
            serviceSelectionView?.tapGetPricing = { [weak self] (selectedService,selectPackage) in
                
                
                guard let self = self else {
                    return
                }
                
                guard !self.DestintionLatti.contains(0.0) else {

                    print(self.DestintionLatti)
                    print(self.DestintionLatti.count)
                    
                    ToastManager.show(title: TaxiConstant.chooseProviderLoc.localized, state: .error)
                    
                    return
                }
                
                print(self.DestintionLatti)
                print(self.DestintionLatti.count)
                
                
                //My location
                let myLocation = CLLocation(latitude: (self.sourceLocationDetail.locationCoordinate?.latitude ?? 0.0), longitude: (self.sourceLocationDetail.locationCoordinate?.longitude ?? 0.0))

                //My buddy's location
                let myBuddysLocation = CLLocation(latitude: (self.destinationLocationDetail.locationCoordinate?.latitude ?? 0.0), longitude: (self.destinationLocationDetail.locationCoordinate?.longitude ?? 0.0))

                //Measuring my distance to my buddy's (in km)
                let distance = myLocation.distance(from: myBuddysLocation) / 1000

                let disStr = String(format: "The distance to my buddy is %.01f", distance)
                
                //Display the result in km
                print(String(format: "The distance to my buddy is %.01f", distance))
                if self.taxitype == .outstation {
                    if (distance) < 100.0 {
                        ToastManager.show(title: TaxiConstant.noservice.localized, state: .error)
                        return
                    }
                }
            
                
                if self.taxitype == .outstation || self.taxitype == .ride {
//                    if self.destinationTextField.text?.trimString().count == 0 {
//                        ToastManager.show(title: TaxiConstant.chooseProviderLoc.localized, state: .error)
//                        return
//                    }
                }
                if self.taxitype == .outstation {
                    if self.outstationType == .oneWay {
                        if self.depart_day == "" || self.depart_time == "" {
                            ToastManager.show(title: TaxiConstant.chooseStartDateTime.localized, state: .error)
                            return
                            
                        }
                    }else{
                        if self.depart_day == "" || self.depart_time == "" {
                            ToastManager.show(title: TaxiConstant.chooseStartDateTime.localized, state: .error)
                            return
                            
                        }else if self.return_day == "" || self.return_time == "" {
                            ToastManager.show(title: TaxiConstant.chooseEndDateTime.localized, state: .error)
                            return
                            
                        }
                    }
                }
                
                print(self.DestintionLatti)
                
                if self.DestintionLatti.contains(0.0) {
                    
                    self.DestintionLatti.removeLast()
                }
                
               
                
                let requestEntity = TaxiReuqestEntity()
                requestEntity.s_latitude = self.sourceLocationDetail.locationCoordinate?.latitude ?? 0.0
                requestEntity.s_longitude = self.sourceLocationDetail.locationCoordinate?.longitude ?? 0.0
                requestEntity.s_address = self.sourceLocationDetail.address
                requestEntity.service_type = selectedService.id
                
                requestEntity.d_address0 = self.DestintionArray
                requestEntity.d_latitude0 = self.DestintionLatti
                requestEntity.d_longitude0 = self.DestintionLangi
                requestEntity.payment_mode = self.paymentMode?.rawValue ?? PaymentType.CASH.rawValue
                requestEntity.rideTypeId = self.rideTypeId ?? 0
                requestEntity.vehicle_type = TaxiServiceType(rawValue: self.taxitype.currentType)

                requestEntity.outstation_type = TaxiTrip(rawValue: self.outstationType.currentTrip)
                requestEntity.rental_package_id = selectPackage?.id
                requestEntity.depart_day = self.depart_day
                requestEntity.depart_time = self.depart_time
                requestEntity.return_day = self.schedulEntity?.return_day
                requestEntity.return_time = self.schedulEntity?.return_time
                
                let estimationVC = TaxiRouter.taxiStoryboard.instantiateViewController(withIdentifier: TaxiConstant.PriceEstimationController) as! PriceEstimationController
                estimationVC.requestEntity = requestEntity
                estimationVC.rideNowDelegate = self
                if !(self.navigationController!.viewControllers.contains(estimationVC)){
                    self.navigationController?.pushViewController(estimationVC, animated:true)
                }
            }
        }
    }
    
    func showRateCard(serviceData: Services) {
        if let _ = self.getRateCardView() {
            rateCardView?.setValues(serviceData: serviceData)
            self.rateCardView?.tapDone = { [weak self] in
                guard let self = self else {
                    return
                }
                if (self.removeView(viewObj: self.rateCardView)) {
                    self.rateCardView = nil
                }
                self.showServiceList()
            }
        }
    }
    
    private func showLoaderView() {
        if let _ = self.getLoaderView() {
            loaderView?.cancelRequestButton.backgroundColor = .taxiColor
            self.loaderView?.onClickCancelRequest = { [weak self] in
                guard let self = self else {
                    return
                }
                self.showCancelTable()
            }
        }
    }
    
    // cancel reasons func
    private func showCancelTable() {
        if self.tableView == nil, let tableView = Bundle.main.loadNibNamed(Constant.CustomTableView, owner: self, options: [:])?.first as? CustomTableView {
            let height = (self.view.frame.height/100)*35
            tableView.frame = CGRect(x: 20, y: (self.view.frame.height/2)-(height/2), width: self.view.frame.width-40, height: height)
            tableView.heading = Constant.chooseReason.localized
            self.tableView = tableView
            var reasonArr:[String] = []
            for reason in self.reasonData ?? [] {
                reasonArr.append(reason.reason ?? "")
            }
            tableView.values = reasonArr
            tableView.show(with: .bottom, completion: nil)
            showDimView(view: tableView)
        }
        self.tableView?.onClickClose = { [weak self] in
            guard let self = self else {
                return
            }
            self.tableView?.superview?.dismissView(onCompletion: {
                self.tableView = nil
            })
        }
        self.tableView?.selectedItem = { [weak self] (selectedReason) in
            guard let self = self else {
                return
            }
            let param: Parameters = [TaxiConstant.id: self.currentRequest?.data?.first?.id  ?? 0,
                                     TaxiConstant.reason: selectedReason]
            self.taxiPresenter?.cancelRequest(param: param)
        }
    }
    
    func showInvoice(with request: RequestData) {
        
        if let _ = self.getInvoiceView() {
            self.invoiceView?.changeButton.addTarget(self, action: #selector(self.changeInvoicePaymentTapped), for: .touchUpInside)
            self.invoiceView?.onClickConfirm = { [weak self] (tipsAmount,isCash) in
                guard let self = self else {
                    return
                }
                self.invoiceView?.dismissView(onCompletion: {
                    self.invoiceView = nil
                })
                if isCash  {
                    self.isInvoiceShowed = true
                    self.showRatingView(with: request)
                }
                else{
                    if request.paid == 1 {
                        self.showRatingView(with: request)
                    }else {
                        let param: Parameters = [TaxiConstant.id: request.id ?? 0,
                                                 TaxiConstant.tips: tipsAmount,
                                                 TaxiConstant.card_id: self.selectedCard.card_id ?? ""]
                        
                        print("param", param)
                        self.taxiPresenter?.invoicePayment(param: param)
                        
                        
                    }
                }
            }
            self.invoiceView?.setValues(values: request)
        }
    }
    
    func showRatingView(with request : RequestData) {
        
        if let requestWallet = request.payment?.wallet, requestWallet > 0 {  //Locally change wallet amount, without hitting profile API
            var walletBalance = AppManager.shared.getUserDetails()?.wallet_balance
            walletBalance = (walletBalance ?? 0) - requestWallet
            AppManager.shared.getUserDetails()?.wallet_balance = walletBalance
        }
        
        if let _ = self.getRatingView() {
            if self.removeView(viewObj: self.invoiceView) {
                self.invoiceView = nil
            }
            self.ratingView?.setValues(color: .taxiColor)
            self.ratingView?.idLabel.text = TaxiConstant.bookingId.localized + ":" + "\(String(describing: request.booking_id ?? ""))"
            if let provider = request.provider {
                self.ratingView?.userNameLabel.text = (provider.first_name ?? "") + (provider.last_name ?? "")
                
                self.ratingView?.userNameImage.sd_setImage(with: URL(string: provider.picture ?? ""), placeholderImage:#imageLiteral(resourceName: "ImagePlaceHolder"),options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
                    // Perform operation.
                    if (error != nil) {
                        // Failed to load image
                        self.ratingView?.userNameImage.image = #imageLiteral(resourceName: "ImagePlaceHolder")
                    } else {
                        // Successful in loading image
                        self.ratingView?.userNameImage.image = image
                    }
                })
            }
            self.ratingView?.onClickSubmit = { [weak self] (rating, comments) in
                guard let self = self else {
                    return
                }
                if (self.currentRequest?.data?.first?.id ?? 0) > 0 {
                    let comment = comments == Constant.leaveComment.localized ? "" : comments
                    let param: Parameters = [TaxiConstant.id: self.currentRequest?.data?.first?.id ?? 0,
                                             TaxiConstant.rating: rating,
                                             TaxiConstant.comment: comment,
                                             TaxiConstant.admin_service_id: request.service_type?.admin_service_id ?? ""]
                    self.taxiPresenter?.ratingToProvider(param: param)
                }
            }
        }
    }
    
    func showRideStatusView(with request : RequestData) {
        if let _ = self.getRideStatusView() {
            if self.rideStatusView != nil {
                self.rideStatusView?.set(values: request)
                let status = TaxiRideStatus(rawValue: request.status ?? "") ?? .none
                setMenuButton()
                if status == .pickedup {
                    floatyButton?.removeFromSuperview()
                    floatyButton = nil
                }
            }
            self.rideStatusView?.onClickCancelOrShareRide =  { [weak self] (isCancel) in
                if isCancel {
                    self?.showCancelTable()
                }else{
                    self?.shareRide()
                }
            }
            // show riding status view
            self.showDetailView(with: request)
        }
    }
    
    private func getRateCardView() -> RateCardView?{
        
        guard let _ = self.rateCardView else {
            if let rateCardView = Bundle.main.loadNibNamed(TaxiConstant.RateCardView, owner: self, options: [:])?.first as? RateCardView {
                self.view.addSubview(rateCardView)
                rateCardView.translatesAutoresizingMaskIntoConstraints = false
                rateCardView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
                rateCardView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
                rateCardView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
                rateCardView.show(with: .bottom, completion: nil)
                rateCardView.layoutIfNeeded()
                self.rateCardView = rateCardView
            }
            return rateCardView
        }
        return self.rateCardView
    }
    
    private func getRideStatusView() -> RideStatusView? {
        guard let _ = self.rideStatusView else {
            if let rideStatusView =  Bundle.main.loadNibNamed(TaxiConstant.RideStatusView, owner: self, options: [:])?.first as? RideStatusView {
                self.view.addSubview(rideStatusView)
                rideStatusView.translatesAutoresizingMaskIntoConstraints = false
                rideStatusView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -10).isActive = true
                rideStatusView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 10).isActive = true
                rideStatusView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -10).isActive = true
                rideStatusView.show(with: .right, completion: nil)
                rideStatusView.layoutIfNeeded()
                self.rideStatusView = rideStatusView
            }
            return rideStatusView
        }
        return self.rideStatusView
    }
    
    private func getRatingView() -> RatingView? {
        guard let _ = self.ratingView else {
            if let ratingView = Bundle.main.loadNibNamed(Constant.RatingView, owner: self, options: [:])?.first as? RatingView {
                self.view.addSubview(ratingView)
                ratingView.translatesAutoresizingMaskIntoConstraints = false
                ratingView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
                ratingView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
                ratingView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
                ratingView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
                ratingView.show(with: .bottom, completion: nil)
                ratingView.layoutIfNeeded()
                self.ratingView = ratingView
            }
            return ratingView
        }
        return self.ratingView
    }
    
    private func getLoaderView() -> LoaderView?{
        guard let _ = self.loaderView else {
            if let loaderView = Bundle.main.loadNibNamed(TaxiConstant.LoaderView, owner: self, options: [:])?.first as? LoaderView {
                self.view.addSubview(loaderView)
                loaderView.translatesAutoresizingMaskIntoConstraints = false
                loaderView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
                loaderView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
                loaderView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
                loaderView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
                loaderView.show(with: .bottom, completion: nil)
                loaderView.layoutIfNeeded()
                self.loaderView = loaderView
            }
            return loaderView
        }
        return self.loaderView
    }
    
    private func getInvoiceView() -> InvoiceView?{
        guard let _ = self.invoiceView else {
            if let invoiceView = Bundle.main.loadNibNamed(TaxiConstant.InvoiceView, owner: self, options: [:])?.first as? InvoiceView {
                self.view.addSubview(invoiceView)
                invoiceView.translatesAutoresizingMaskIntoConstraints = false
                invoiceView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
                invoiceView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
                invoiceView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
                invoiceView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
                invoiceView.show(with: .bottom, completion: nil)
                invoiceView.layoutIfNeeded()
                self.invoiceView = invoiceView
            }
            return invoiceView
        }
        return self.invoiceView
    }
    
    private func getRideDetailView() -> RideDetailView? {
        guard let _ = self.rideDetailView else {
            if let rideDetailView =  Bundle.main.loadNibNamed(TaxiConstant.RideDetailView, owner: self, options: [:])?.first as? RideDetailView {
                self.view.addSubview(rideDetailView)
                rideDetailView.translatesAutoresizingMaskIntoConstraints = false
                rideDetailView.topAnchor.constraint(equalTo: self.backButton.bottomAnchor, constant: 15).isActive = true
                rideDetailView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 10).isActive = true
                rideDetailView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -10).isActive = true
                rideDetailView.show(with: .right, completion: nil)
                rideDetailView.layoutIfNeeded()
                self.rideDetailView = rideDetailView
            }
            return rideDetailView
        }
        return self.rideDetailView
    }
    
    private func getServiceListView() -> ServiceSelectionView?{
        
        guard let _ = self.serviceSelectionView else {
            if let serviceSelectionView =  Bundle.main.loadNibNamed(TaxiConstant.ServiceSelectionView, owner: self, options: [:])?.first as? ServiceSelectionView  {
                self.view.addSubview(serviceSelectionView)
                serviceSelectionView.translatesAutoresizingMaskIntoConstraints = false
                serviceSelectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
                serviceSelectionView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
                serviceSelectionView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
                serviceSelectionView.show(with: .bottom, completion: nil)
                self.serviceSelectionView = serviceSelectionView
            }
            return serviceSelectionView
        }
        return self.serviceSelectionView
    }
    
    func showDetailView(with request : RequestData) {
        if let _ = self.getRideDetailView() {
            self.rideDetailView?.extendTripButton.addTarget(self, action: #selector(self.extendTripSelect), for: .touchUpInside)
            
            self.rideDetailView?.mapnavigationvutton.addTarget(self, action: #selector(NavigationLocation), for: .touchUpInside)
            
            
            self.rideDetailView?.set(values: request)
        }
    }
    
    
    
    @objc func NavigationLocation(sender: UIButton){
        
        redirectToGoogleMap()
        
    }
    
    
    private func handleSearch() {
        if self.removeView(viewObj: self.rideStatusView) {
            self.rideStatusView = nil
        }
        if self.removeView(viewObj: self.serviceSelectionView) {
            self.serviceSelectionView = nil
        }
        if self.removeView(viewObj: self.invoiceView) {
            self.invoiceView = nil
        }
        if self.removeView(viewObj: self.ratingView) {
            self.ratingView = nil
        }
        if self.removeView(viewObj: self.rideDetailView) {
            self.rideDetailView = nil
        }
        self.tableView?.superview?.dismissView(onCompletion: {
            self.tableView = nil
        })
        self.showLoaderView()
    }
    
    private func handleOnRide(with status: String) {
        if self.removeView(viewObj: self.serviceSelectionView) {
            self.serviceSelectionView = nil
        }
        if self.removeView(viewObj: self.loaderView) {
            self.loaderView = nil
        }
        if self.removeView(viewObj: self.invoiceView) {
            self.invoiceView = nil
        }
        if self.removeView(viewObj: self.ratingView) {
            self.ratingView = nil
        }
        self.tableView?.superview?.dismissView(onCompletion: {
            self.tableView = nil
        })
        DispatchQueue.main.async {
            
            self.xmapView?.drawPolyLineFromSourceToDestination(source: self.sourceLocationDetail.locationCoordinate ?? APPConstant.defaultMapLocation, destination: self.destinationLocationDetail.locationCoordinate ?? APPConstant.defaultMapLocation, lineColor: .taxiColor)
            
            
            self.xmapView?.showCurrentLocation()
            
            self.xmapView?.enableProviderMovement(providerId: self.currentRequest?.data?.first?.provider?.id ?? 0)
            self.showRideStatusView(with: self.currentRequest?.data?.first ?? RequestData())
        }
    }
    
    private func handleComplete() {
        
        if self.removeView(viewObj: self.serviceSelectionView) {
            self.serviceSelectionView = nil
        }
        if self.removeView(viewObj: self.loaderView) {
            self.loaderView = nil
        }
        if self.removeView(viewObj: self.rideStatusView) {
            self.rideStatusView = nil
        }
        if self.removeView(viewObj: self.rideDetailView) {
            self.rideDetailView = nil
        }
        self.tableView?.superview?.dismissView(onCompletion: {
            self.tableView = nil
        })
        self.sosButton?.removeFromSuperview()
        self.sosButton = nil
        self.floatyButton?.removeFromSuperview()
        self.floatyButton = nil
        self.xmapView?.enableProviderMovement(providerId: 0)
        
        if let requestDetails = self.currentRequest?.data?.first {
            
            if let invoiceShowed = self.isInvoiceShowed,invoiceShowed == true {
                
                if self.removeView(viewObj: self.invoiceView) {
                    self.invoiceView = nil
                }
                self.showRatingView(with: requestDetails)
            }else {
                
                self.showInvoice(with: requestDetails)
                
            }
        }
    }
    
    private  func handleDropped() {
        if self.removeView(viewObj: self.serviceSelectionView) {
            self.serviceSelectionView = nil
        }
        if self.removeView(viewObj: self.rideStatusView) {
            self.rideStatusView = nil
        }
        if self.removeView(viewObj: self.loaderView) {
            self.loaderView = nil
        }
        
        if self.removeView(viewObj: self.rideDetailView) {
            self.rideDetailView = nil
        }
        if self.removeView(viewObj: self.ratingView) {
            self.ratingView = nil
        }
        if self.removeView(viewObj: self.tableView?.superview) {
            self.tableView = nil
        }
        self.sosButton?.removeFromSuperview()
        self.sosButton = nil
        self.floatyButton?.removeFromSuperview()
        self.floatyButton = nil
        self.xmapView?.enableProviderMovement(providerId: 0)
        self.showInvoice(with: self.currentRequest?.data?.first ?? RequestData())
    }
    
    private func removeAllView() {
         newRequest =  false
        UserDefaults.standard.set(newRequest, forKey: Constant.newRequestHideSearch)
        
        if self.removeView(viewObj: self.rideStatusView) {
            self.rideStatusView = nil
        }
        if self.removeView(viewObj: self.loaderView) {
            self.loaderView = nil
        }
        if self.removeView(viewObj: self.invoiceView) {
            self.invoiceView = nil
        }
        if self.removeView(viewObj: self.ratingView) {
            self.ratingView = nil
        }
        if self.removeView(viewObj: self.rideDetailView) {
            self.rideDetailView = nil
        }
        if self.removeView(viewObj: self.tableView?.superview) {
            self.tableView = nil
        }
        self.sosButton?.removeFromSuperview()
        self.sosButton = nil
        self.floatyButton?.removeFromSuperview()
        self.floatyButton = nil
        self.xmapView?.clearAll()
        self.addressBGView.isHidden = false
        self.destinationLocationDetail = SourceDestinationLocation()
        self.getServiceListAPI()
    }
    
    private func setSourceDestinationMarker() {
       
        if self.sourceLocationDetail.locationCoordinate != nil{
           
        self.xmapView?.setSourceLocationMarker(sourceCoordinate: self.sourceLocationDetail.locationCoordinate ?? APPConstant.defaultMapLocation, marker: UIImage(named: Constant.sourcePin) ?? UIImage())
        }
        // self.xmapView?.setDestinationLocationMarker(destinationCoordinate: self.destinationLocationDetail.locationCoordinate ?? APPConstant.defaultMapLocation, marker: UIImage(named: Constant.destinationPin) ?? UIImage())
        for mapPin in 0..<DestintionLattiLangi.count{
            
            if IspolylineChange == true{
                
                if DestintionLattiLangi.count == 2{
                    
                    self.xmapView?.setDestinationLocationMarker(destinationCoordinate: DestintionLattiLangi[mapPin] , marker: mapPin == 1 ? UIImage(named: Constant.destinationPin) ?? UIImage() : UIImage(named: Constant.destinationPin) ?? UIImage(), titleStr: self.DestintionArray[mapPin])
                    
                    
                }else if DestintionLattiLangi.count == 3{
                    
                    self.xmapView?.setDestinationLocationMarker(destinationCoordinate: DestintionLattiLangi[mapPin] , marker: mapPin == 2 ? UIImage(named: Constant.destinationPin) ?? UIImage() : UIImage(named: Constant.destinationPin) ?? UIImage(), titleStr: self.DestintionArray[mapPin])
                }else{
                    
                    self.xmapView?.setDestinationLocationMarker(destinationCoordinate: DestintionLattiLangi[mapPin] , marker: mapPin == 0 ? UIImage(named: Constant.destinationPin) ?? UIImage() : UIImage(named: Constant.destinationPin) ?? UIImage(), titleStr: self.DestintionArray[mapPin])
                }
                 
            }else {
                
                self.xmapView?.setDestinationLocationMarker(destinationCoordinate: DestintionLattiLangi[mapPin] , marker: mapPin == 0 ? UIImage(named: Constant.destinationPin) ?? UIImage() : UIImage(named: Constant.destinationPin) ?? UIImage(), titleStr: self.DestintionArray[mapPin])
            }
           
            if mapPin >= 1 {
                
                let waypoint = DestintionLattiLangi.filter{$0.latitude != 0.0 && $0.longitude != 0.0}
              
                self.xmapView?.drawPolyLineFromSourceToDestination(source:DestintionLattiLangi[mapPin - 1], destination: DestintionLattiLangi[mapPin],waypoints : waypoint, lineColor: .taxiColor)
            }else{
                
                let waypoint = DestintionLattiLangi.filter{$0.latitude != 0.0 && $0.longitude != 0.0}
                self.xmapView?.drawPolyLineFromSourceToDestination(source: self.sourceLocationDetail.locationCoordinate ?? APPConstant.defaultMapLocation, destination: DestintionLattiLangi[mapPin],waypoints : waypoint,lineColor: .taxiColor)
            }
            
           if mapPin == 0{
            
            if DestintionLattiLangi.count == 3{
                
                self.xmapView?.drawPolyLineFromSourceToDestination(source: self.sourceLocationDetail.locationCoordinate ?? APPConstant.defaultMapLocation, destination: DestintionLattiLangi[2], lineColor: .taxiColor)
            }
            if DestintionLattiLangi.count == 2{
                
                self.xmapView?.drawPolyLineFromSourceToDestination(source: self.sourceLocationDetail.locationCoordinate ?? APPConstant.defaultMapLocation, destination: DestintionLattiLangi[1], lineColor: .taxiColor)
            }
            if DestintionLattiLangi.count == 1{
                
                self.xmapView?.drawPolyLineFromSourceToDestination(source: self.sourceLocationDetail.locationCoordinate ?? APPConstant.defaultMapLocation, destination: DestintionLattiLangi[0], lineColor: .taxiColor)
            }
            }
           else if mapPin == 1{
            
              if DestintionLattiLangi.count == 3{
                self.xmapView?.drawPolyLineFromSourceToDestination(source: DestintionLattiLangi[2], destination: DestintionLattiLangi[1], lineColor: .taxiColor)
              }
              if DestintionLattiLangi.count == 2{
                
                self.xmapView?.drawPolyLineFromSourceToDestination(source: DestintionLattiLangi[1], destination: DestintionLattiLangi[0], lineColor: .taxiColor)
              }
            }
           else if mapPin == 2{
            
              if DestintionLattiLangi.count == 3{
                
                self.xmapView?.drawPolyLineFromSourceToDestination(source: DestintionLattiLangi[1], destination: DestintionLattiLangi[0], lineColor: .taxiColor)
              }
            }
        }
       
    }
    
    private func removeView(viewObj: UIView?) -> Bool {
        if let views = viewObj {
            views.removeFromSuperview()
            return true
        }
        return false
    }
    
    private func shareRide() {
        let rideDetail = currentRequest?.data?.first
        let driverName = (rideDetail?.provider?.first_name ?? .empty).giveSpace + (rideDetail?.provider?.last_name ?? .empty)
        let vehicleNameNo = "\(rideDetail?.service_type?.vehicle?.vehicle_model ?? .empty) " + "(" + "\(rideDetail?.service_type?.vehicle?.vehicle_no ?? .empty)" + ")"
        
        let mapFormat = "http://maps.google.com/maps?q=loc:\(XCurrentLocation.shared.latitude ?? 0),\(XCurrentLocation.shared.longitude ?? 0)"
        let addressDetails = TaxiConstant.tookFrom + " \(rideDetail?.s_address ?? .empty) " + TaxiConstant.to + " \(rideDetail?.d_address ?? .empty)"
        
        var message = TaxiConstant.shareRideInitialContent + " \(driverName) " + TaxiConstant.shareContentIn.giveSpace + vehicleNameNo.giveSpace
        message += TaxiConstant.geolocation + " \(mapFormat)" + "\n"
        message += addressDetails
        
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [message], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
}

//MARK:- Textfield Delegate



extension TaxiHomeViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let vc = TaxiRouter.taxiStoryboard.instantiateViewController(withIdentifier: TaxiConstant.LocationSelectionController) as! LocationSelectionController
        vc.locationDelegate = self
        vc.isSource = textField == self.sourceTextField
        self.navigationController?.pushViewController(vc, animated: true)
        return false
    }
    
    private func serviceAPIList(type: TaxiServiceType,loader: Bool){
        let tempId = "\(AppManager.shared.getSelectedServices()?.menu_type_id ?? 0)"
        let param: Parameters = [TaxiConstant.type: tempId,
                                 AccountConstant.latitude: sourceLocationDetail.locationCoordinate?.latitude ?? String.empty,
                                 AccountConstant.longitude: sourceLocationDetail.locationCoordinate?.longitude ?? String.empty,
                                 LoginConstant.city_id: guestAccountCity,
                                 TaxiConstant.Pvehicle_type: type.currentType]

        self.taxiPresenter?.getServiceList(param: param,showLoader: loader)
    }
}

//MARK:- Location delegate

extension  TaxiHomeViewController: LocationDelegate {
    func selectedLocation(isSource: Bool, addressDetails: SourceDestinationLocation) {
        if isSource {
            self.sourceLocationDetail = addressDetails
            let tempId = "\(AppManager.shared.getSelectedServices()?.menu_type_id ?? 0)"

            let location = sourceLocationDetail.locationCoordinate
            let param: Parameters = [TaxiConstant.type: tempId,
                                     AccountConstant.latitude: location?.latitude ?? String.empty,
                                     AccountConstant.longitude: location?.longitude ?? String.empty,
                                     LoginConstant.city_id: guestAccountCity,
                                     TaxiConstant.Pvehicle_type: taxitype.currentType]

            self.taxiPresenter?.getServiceList(param: param, showLoader: false)
        }else{
            self.destinationLocationDetail = addressDetails
        }
    }
}

//MARK:- API calls

extension TaxiHomeViewController: TaxiPresenterToTaxiViewProtocol {
    
    func checkTaxiRequest(requestEntity: Request) {
        if let requestData = requestEntity.responseData?.data, requestData.count > 0 {
            self.currentRequest = requestEntity.responseData
            self.socketAndBgTaskSetUp()
            DispatchQueue.main.async {
                self.handleRequest()
            }
        }else{
            self.currentRequest = requestEntity.responseData
            
            newRequest = UserDefaults.standard.value(forKey: Constant.newRequestHideSearch) as? Bool ?? false

            if newRequest {
                self.riderStatus = .none
                self.removeAllView()
            }
            
            
            if isFromOrderPage {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func getServiceList(serviceEntity: ServiceListEntity) {
        
        self.serviceList = serviceEntity.responseData?.services ?? []
        self.packageArr = serviceEntity.responseData?.services?.first?.price_details?.rental_packages ?? []
            
        if let providers = serviceEntity.responseData?.providers,providers.count > 0 && self.currentRequest?.data?.count == 0 {
            let markerImage = UIImageView()
            
            for providerMarker in providers {
                if let imageUrl = URL(string: serviceEntity.responseData?.services?.first?.vehicle_marker ?? "")  {
                    markerImage.sd_setImage(with: imageUrl, placeholderImage: #imageLiteral(resourceName: "sourcePin"))
                    
                    markerImage.contentMode = .scaleAspectFit
                }
                let mapMarker = MarkerDetails(image: markerImage.image ?? #imageLiteral(resourceName: "sourcePin"), position:LocationCoordinate(latitude: providerMarker.latitude ?? 0.0, longitude: providerMarker.longitude ?? 0.0))
//                self.xmapView?.addMarker(markers: mapMarker)
            }
        }
        DispatchQueue.main.async {
            if (self.currentRequest?.data?.count ?? 0) == 0 || self.riderStatus == .none {
                self.showServiceList()
            }
        }
    }
    
    // get cancel reasons
    func getReasonsResponse(reasonEntity: ReasonEntity) {
        self.reasonData = reasonEntity.responseData ?? []
    }
    
    func ratingToProviderSuccess(requestEntity: Request) {
        BackGroundRequestManager.share.resetBackGroudTask()
        self.currentRequest = requestEntity.responseData
        self.isInvoiceShowed = false
        handleRequest()
        ToastManager.show(title: Constant.RatingToast, state: .success)
        socketAndBgTaskSetUp()
        paymentMode = PaymentType(rawValue: PaymentType.CASH.rawValue)
        self.navigationController?.popViewController(animated: true)
        if isFromOrderPage {
           // self.navigationController?.popViewController(animated: true)
        }else{
            if (self.currentRequest?.data?.count ?? 0) == 0 {
                self.getServiceListAPI()
            }
        }
        
    }
    
    func invoicePaymentSuccess(requestEntity: Request) {
//        ToastManager.show(title: requestEntity.message ?? "", state: .success)
        ToastManager.show(title: "Paid succcessfully", state: .success)
        self.isInvoiceShowed = true
        checkRequestApiCall()
    }
    
    func cancelRequestSuccess(requestEntity: Request) {
        self.tableView?.superview?.dismissView(onCompletion: {
            self.tableView = nil
        })
        ToastManager.show(title: requestEntity.message ?? "", state: .success)
        
        paymentMode = PaymentType(rawValue: PaymentType.CASH.rawValue)
        self.currentRequest = requestEntity.responseData
        if isFromOrderPage {
            self.navigationController?.popViewController(animated: true)
        }else{
            self.removeAllView()
        }
        checkRequestApiCall()
    }
    
    func failureResponse(failureData: Data) {
        if
            let utf8Text = String(data: failureData, encoding: .utf8),
            let messageDic = AppUtils.shared.stringToDictionary(text: utf8Text),
            let message =  messageDic[Constant.message] as? String {
            if message == TaxiConstant.rideAlreadyCancel {
                self.tableView?.superview?.dismissView(onCompletion: {
                    self.tableView = nil
                })
                paymentMode = PaymentType(rawValue: PaymentType.CASH.rawValue)
                
                socketAndBgTaskSetUp()
                if isFromOrderPage {
                    self.navigationController?.popViewController(animated: true)
                }else{
                    self.removeAllView()
                }
            }
        }
    }
    
    // update payment
    func updatePaymentSuccess(requestEntity: Request) {
        ToastManager.show(title: requestEntity.message ?? "", state: .success)
        checkRequestApiCall()
    }
    
    func extendTripSuccess(requestEntity: Request) {
        ToastManager.show(title: requestEntity.message ?? "", state: .success)
        self.currentRequest = requestEntity.responseData
        checkRequestApiCall()
    }
    
    func sendRequestSuccess(requestEntity: Request) {
        ToastManager.show(title: requestEntity.message ?? "", state: .success)
        checkRequestApiCall()
        serviceList = nil
        navigationController?.popViewController(animated: true)
    }
}

extension TaxiHomeViewController:  RideNowDelegate {
    
    func onRideCreated() {
        
        checkRequestApiCall()
        newRequest =  true
        UserDefaults.standard.set(newRequest, forKey: Constant.newRequestHideSearch)

        serviceList = nil
    }
}


extension TaxiHomeViewController : XmapLocationUpdateProtocol{
    func locationUpdated(location: XCurrentLocation) {
        print("current location : \(location)")
    }
    
}



extension TaxiHomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  54//UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
             if DestintionArray.count == 3{
                   if indexPath.row == 0{
                       UserDefaults.standard.set(0, forKey:"INDEX")
                   }else if indexPath.row == 1{
                       UserDefaults.standard.set(1, forKey:"INDEX")
                   } else{
                     UserDefaults.standard.set(2, forKey:"INDEX")
                   }
             }else{
                UserDefaults.standard.set(indexPath.row, forKey:"INDEX")
             }
              IsClickLocation = true
              let vc = TaxiRouter.taxiStoryboard.instantiateViewController(withIdentifier: TaxiConstant.LocationSelectionController) as! LocationSelectionController
              vc.locationDelegate = self
              vc.isSource = false
               
              if indexPath.row != 0{
                vc.IsAddaStop = false
                //vc.locationTextField.placeholder = TaxiConstant.Addstops.localized
              }else{
                vc.IsAddaStop = true
                //vc.locationTextField.placeholder = TaxiConstant.enterDestinationStop.localized
               }
              self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func CheckCellText() -> Bool{
        for index in 0..<DestintionArray.count{
            
            let indexPath = IndexPath(row: index, section: 0)
            let cell:DestinationTableViewCell=(self.destinationTableview.cellForRow(at: indexPath) as? DestinationTableViewCell)!
            if let textField = cell.DestinationTf {
                
               
                    if textField.tag == index{
                        if  textField.text == TaxiConstant.enterDestination.localized || textField.text == "" {
                            ToastManager.show(title: TaxiConstant.enterDestination.localized , state: .error)
                            DestintionArray[index] =  TaxiConstant.enterDestination.localized
                            return false
                        }
                    }
                    if textField.tag == index {
                        DestintionArray[index] = (textField.text ?? TaxiConstant.enterDestination.localized)
                    }
                
               
            }
            
        }
        return true
    }
}
extension TaxiHomeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("ARRAYDATA",DestintionArray)
        return DestintionArray.count
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let cell = self.destinationTableview.dequeueReusableCell(withIdentifier: HomeConstant.DestinationTableViewCell, for: indexPath) as! DestinationTableViewCell
        cell.selectionStyle = .none
        if  DestintionArray[indexPath.row] == TaxiConstant.enterDestination.localized {
            cell.BackView.backgroundColor = .white
        }else{
            cell.BackView.backgroundColor = .veryLightGray
        }
        cell.DestinationTf.textColor = DestintionArray[indexPath.row] == TaxiConstant.enterDestination.localized ? .appPrimaryColor : .black
        
        if DestintionArray.count == 3{
            if indexPath.row == 0{
                cell.DestinationTf.text = DestintionArray[0] //== TaxiConstant.enterDestination.localized ? TaxiConstant.Addstops.localized : DestintionArray[indexPath.row]
            }else if indexPath.row == 1{
                cell.DestinationTf.text = DestintionArray[1] == TaxiConstant.enterDestination.localized ? TaxiConstant.Addstops.localized : DestintionArray[1]
            } else{
                 cell.DestinationTf.text = DestintionArray[2] == TaxiConstant.enterDestination.localized ? TaxiConstant.Addstops.localized : DestintionArray[2]
            }
        }else if DestintionArray.count == 2{
            if indexPath.row == 0{
                cell.DestinationTf.text = DestintionArray[indexPath.row] //== TaxiConstant.enterDestination.localized ? TaxiConstant.Addstops.localized : DestintionArray[indexPath.row]
            }else{
                 cell.DestinationTf.text = DestintionArray[indexPath.row] == TaxiConstant.enterDestination.localized ? TaxiConstant.Addstops.localized : DestintionArray[indexPath.row]
            }
        }else{
            if indexPath.row == 0{
                cell.DestinationTf.text = DestintionArray[indexPath.row] //== TaxiConstant.enterDestination.localized ? TaxiConstant.Addstops.localized : DestintionArray[indexPath.row]
            }else{
                 cell.DestinationTf.text = DestintionArray[indexPath.row] == TaxiConstant.enterDestination.localized ? TaxiConstant.Addstops.localized : DestintionArray[indexPath.row]
            }

        }
        
  
        cell.addMinus.tag = indexPath.row
        cell.remove.tag = indexPath.row
        cell.DestinationTf.tag = indexPath.row
        
        
              if DestintionArray.count > 2{
                  centerAddBtn.isHidden = false
              }else{
                  centerAddBtn.isHidden = false
                  
              }
              
             // if indexPath.row == DestintionArray.count - 1{
         if indexPath.row == 0 {
             if DestintionArray.count > 1 && DestintionArray.count >= 2  {
                   cell.addMinus.isHidden = true
                   cell.remove.isHidden = true
                   cell.destinatioPinPointer.image  = #imageLiteral(resourceName: "destinationPin")
                   }else{
                      cell.addMinus.isHidden = true
                      cell.remove.isHidden = true
                      cell.destinatioPinPointer.image = #imageLiteral(resourceName: "destinationPin")
                  }
              }else{
                   cell.addMinus.isHidden = true
                   cell.remove.isHidden = false
                   cell.likeBtn.isHidden = true
//                   cell.destinationpinleading.constant = 20
                   cell.destinatioPinPointer.image = #imageLiteral(resourceName: "destinationPin")
              }
        
               if DestintionArray.count == 3{
                    centerAddBtn.isHidden = true
                }else{
                    centerAddBtn.isHidden = false
                }
        
 
        centerAddBtn.addTarget(self, action: #selector(addDelete), for: .touchUpInside)
        cell.addMinus.addTarget(self, action: #selector(addDelete), for: .touchUpInside)
        cell.remove.addTarget(self, action: #selector(RemoveWill), for: .touchUpInside)
//        cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
        return cell
    }
    
    @objc func addDelete(_sender:UIButton){
        
         DestintionArray.append(TaxiConstant.enterDestination.localized)
         DestintionLattiLangi.append(CLLocationCoordinate2D.init(latitude: 0.0, longitude: 0.0))
         DestintionLangi.append(0.00)
         DestintionLatti.append(0.00)
         destinationTableview.reloadData()
         let get = DestintionArray.count*53
        // self.DestinationViewConstrain.constant = CGFloat(get)
         self.addressViewConstrain.constant =  CGFloat(102 + (get - 53))
    }
    
    @objc func RemoveWill(_sender:UIButton){
        
        if  !DestintionArray.contains(TaxiConstant.enterDestination.localized) {
                Clear()
            }
        if DestintionArray.count == 3{
            if _sender.tag == 0{
                //UserDefaults.standard.set(0, forKey:"INDEX")
                DestintionArray.remove(at:0 )
                DestintionLattiLangi.remove(at:0)
                DestintionLangi.remove(at:0 )
                DestintionLatti.remove(at:0 )
            }else if _sender.tag == 2{
                //UserDefaults.standard.set(2, forKey:"INDEX")
                DestintionArray.remove(at:2 )
                DestintionLattiLangi.remove(at:2)
                DestintionLangi.remove(at:2 )
                DestintionLatti.remove(at:2 )
            } else{
                //UserDefaults.standard.set(1, forKey:"INDEX")
                DestintionArray.remove(at:1 )
                DestintionLattiLangi.remove(at:1)
                DestintionLangi.remove(at:1 )
                DestintionLatti.remove(at:1 )
            }
          }else{
            
            DestintionArray.remove(at:_sender.tag )
            DestintionLattiLangi.remove(at:_sender.tag)
            DestintionLangi.remove(at:_sender.tag )
            DestintionLatti.remove(at:_sender.tag )
          }
        
        //if DestintionArray
       
//         DestintionArray.remove(at:_sender.tag )
//         DestintionLattiLangi.remove(at:_sender.tag)
//         DestintionLangi.remove(at:_sender.tag )
//         DestintionLatti.remove(at:_sender.tag )
         destinationTableview.reloadData()
         let get = DestintionArray.count*53
         //self.DestinationViewConstrain.constant = CGFloat(get)
         self.addressViewConstrain.constant =  CGFloat(102 + (get - 53))
        
         self.setSourceDestinationMarker()
         
        
    }
    
    func Clear(){
       // print(DestintionArray)
       // if self.completeStatus  == true {
     // if  !DestintionArray.contains(TaxiConstant.enterDestination.localized) {
     //   if self.xmapView != nil {
        // if self.completeStatus  != true {
            self.xmapView?.currentPolyLine.map = nil
            self.xmapView?.currentPolyLine = nil
            animationPolyline?.map = nil
            animationPolyline = nil
            self.xmapView?.sourceMarker.map = nil
            self.xmapView?.sourceMarker = nil
            self.xmapView?.destinationMarker.map = nil
            self.xmapView?.destinationMarker = nil
            self.xmapView?.clear()
            
            //}
       // }
        //}
    }
    
   
    
    
    
    
}

    
    
    
    

